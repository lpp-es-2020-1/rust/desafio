use std::{thread, time};
use std::sync::{Mutex, Arc};
use rand;
use std::time::Instant;

struct Filosofo {
    nome: String,
    garfo_esquerda: usize,
    garfo_direita: usize,
}
 
impl Filosofo {
    fn new(nome: &str, garfo_esquerda: usize, garfo_direita: usize) -> Filosofo {
        Filosofo {
            nome: nome.to_string(),
            garfo_esquerda: garfo_esquerda,
            garfo_direita: garfo_direita,
        }
    }
 
    fn eat(&self, mesa: &Mesa) {
        let comeco = Instant::now();
        let mut duration = comeco.elapsed();
        let mut cont = 0;
        while duration.as_millis() < 30000 {
            let _garfo_esquerda = mesa.garfo[self.garfo_esquerda].lock().unwrap();
            let _garfo_direita = mesa.garfo[self.garfo_direita].lock().unwrap();
            let n1: u64 = rand::random::<u64>();
            let n2: u64 = rand::random::<u64>();
            let tempo_espera = time::Duration::from_millis(n1 % 30);
            let tempo_espera2 = time::Duration::from_millis(n2 %30);
    
            thread::sleep(tempo_espera);
            thread::sleep(tempo_espera2);
            duration = comeco.elapsed();
            cont = cont + 1;
        }
        println!("{} comeu {} vezes.", self.nome, cont)
    }
}
 
struct Mesa {
    garfo: Vec<Mutex<()>>,
}
 
fn main() {
    let mesa = Arc::new(Mesa { garfo: vec![
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
        Mutex::new(()),
    ]});
 
    let filosofos = vec![
        //~150
        Filosofo::new("Felipe", 0, 1),
        //~540
        Filosofo::new("Emanuel", 1, 2),
        //~30
        Filosofo::new("Italo", 3, 2),
        //~40
        Filosofo::new("Marcelo", 3, 4),
        //~50
        Filosofo::new("Arthur", 0, 4),
    ];
    let handles: Vec<_> = filosofos.into_iter().map(|p| {
        let mesa = mesa.clone();
 
        thread::spawn(move || {
            p.eat(&mesa);
        })
    }).collect();
 
    for h in handles {
        h.join().unwrap();
    }
}
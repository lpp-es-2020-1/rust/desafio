#![allow(unused_variables)]
use std::fs;
use std::time::Instant;
use std::thread;
use std::sync::{Arc, Mutex};
static mut CONTENTS1: &'static str = "default value";
static mut CONTENTS2: &'static str = "default value";
fn teste_sem_thread(contents1bytes: &[u8], contents2bytes: &[u8]) {
    let comeco = Instant::now();
    let mut count = 0;
    let mut diferencas = 0;
    for seq in contents1bytes {
        if *seq != contents2bytes[count] {
            diferencas = diferencas + 1;
        }
         count = count + 1;
    }
    
    println!("Numero de diferenças: {}", diferencas.to_string());
    
    let duration = comeco.elapsed();
    println!("Tempo de execução: {}", duration.as_millis());

}

fn teste_com_thread(contents1bytesthread: &'static[u8], contents2bytesthread: &'static[u8]) {
    let comecothread = Instant::now();
    let tamanho = contents1bytesthread.len();
    let counter = Arc::new(Mutex::new(0));

    let counter1 = Arc::clone(&counter);
    let counter2 = Arc::clone(&counter);
    let counter3 = Arc::clone(&counter);
    let counter4 = Arc::clone(&counter);

    let handle = thread::spawn(move || {
        let mut num = counter1.lock().unwrap();
        let contents1bytesparte1 = &contents1bytesthread[0 .. tamanho / 4];
        let contents2bytesparte1 = &contents2bytesthread[0.. tamanho / 4];
        let mut count1 = 0;
        let mut diferencas1 = 0;
        for seq in contents1bytesparte1 {
            if *seq != contents2bytesparte1[count1] {
                diferencas1 = diferencas1 + 1;
            }
            count1 = count1 + 1;
        }
        *num += diferencas1;
    });
    
    let handle2 = thread::spawn(move || {
        let mut num = counter2.lock().unwrap();
        let contents1bytesparte2 = &contents1bytesthread[(tamanho / 4)+1 .. 2 * tamanho / 4];
        let contents2bytesparte2 = &contents2bytesthread[(tamanho / 4)+1.. 2 * tamanho / 4];
        let mut count2 = 0;
        let mut diferencas2 = 0;
        for seq in contents1bytesparte2 {
            if *seq != contents2bytesparte2[count2] {
                diferencas2 = diferencas2 + 1;
            }
            count2 = count2 + 1;
        }
        *num += diferencas2;
    });

    let handle3 = thread::spawn(move || {
        let mut num = counter3.lock().unwrap();
        let contents1bytesparte3 = &contents1bytesthread[(2 * tamanho / 4)+1 .. 3 * tamanho / 4];
        let contents2bytesparte3 = &contents2bytesthread[(2 * tamanho / 4)+1.. 3 * tamanho / 4];
        let mut count3 = 0;
        let mut diferencas3 = 0;
        for seq in contents1bytesparte3 {
            if *seq != contents2bytesparte3[count3] {
                diferencas3 = diferencas3 + 1;
            }
            count3 = count3 + 1;
        }
        *num += diferencas3;
    });

    
    let handle4 = thread::spawn(move || {
        let mut num = counter4.lock().unwrap();
        let contents1bytesparte4 = &contents1bytesthread[(3 * tamanho / 4)+1 .. tamanho];
        let contents2bytesparte4 = &contents2bytesthread[(3 * tamanho / 4)+1.. tamanho];
        let mut count4 = 0;
        let mut diferencas4 = 0;
        for seq in contents1bytesparte4 {
            if *seq != contents2bytesparte4[count4] {
                diferencas4 = diferencas4 + 1;
            }
            count4 = count4 + 1;
        }
        *num += diferencas4;
    });

    handle.join().unwrap();
    handle2.join().unwrap();
    handle3.join().unwrap();
    handle4.join().unwrap();

    println!("Numero de diferenças: {}", *counter.lock().unwrap());
    let duration = comecothread.elapsed();
    println!("Tempo de execução: {}", duration.as_millis());

}

fn main() {
    let filename1 = "Primeira Sequencia.txt";
    let filename2 = "Segunda Sequencia.txt";
    let contents1bytes:&[u8];
        let contents2bytes:&[u8];
    unsafe {
            CONTENTS1 = Box::leak(fs::read_to_string(filename1)
            .expect("Something went wrong reading the file").into_boxed_str());
            CONTENTS2 = Box::leak(fs::read_to_string(filename2)
            .expect("Something went wrong reading the file").into_boxed_str());
            contents1bytes = CONTENTS1.as_bytes();
            contents2bytes = CONTENTS2.as_bytes();
    }
    println!("Teste sem uso de thread");
    teste_sem_thread(&contents1bytes, &contents2bytes);    
    println!("Teste com uso de 4 threads");
    teste_com_thread(&contents1bytes, &contents2bytes);
}

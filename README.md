# Documentações Linguagem e Desafio

# Linguagem Rust

# Desafio de comparação de Strings

### Sumário
1.Linguagem Rust

1.1.Introdução

1.2.História

1.3.Características Técnicas

2 Desafio Rust vs Go – Diferenças de DNA

2.1.Desafio

2.2.Conceitos

2.3.Instalação

2.4.Realizando Testes

2.5.Resultado

3.Referencias

4.Código-Fonte

## 1. Linguagem Rust
### 1.1. Introdução

Rust é uma linguagem que permite escrever código de maneira rápida e confiável. Tendo ferramentas de alto nível e controles de memoria de baixo nível.  Rust pode ser usada por times de desenvolvimento, estudantes, companhias ou desenvolvedores Open Soucer. 
Dentre suas vantagens que o diferem de outras linguagens de baixo nível, está o compilador que fornece um excelente controle de erros e permite evitar que bugs passem para o produto final, reduzindo tempo de manutenção e aumentando tempo de desenvolvimento. Outras são velocidade e estabilidade.
Rust tem foco na rapidez e gerenciamento de memoria eficiente. Não tem runtime ou garbage collector, focando em serviços críticos, rodar sistemas embarcados e integrar com outras linguagens.
Rust foca na confiabilidade, usando o compilador e o modelo de domínio para garantir a segurança de memória e concorrência. Identificando muitos erros durante a compilação. Rust tem muitas utilidades, pode ser usado para criar linhas de comando, WebAssembly, serviços de rede ou serviços embarcados.

### 1.2. História
Rust foi inicialmente um projeto pessoal de Graydon Hoare em 2006. A partir de 2009 a empresa Mozila começou a financiar o projeto e em 2010 fez o anuncio da nova linguagem. A primeira versão Pré-alpha foi lançada em janeiro de 2012 e a versão 1.0 foi lançada em maio de 2015. Desde então atualizações estáveis tem sido lançadas de 6 em 6 semanas.
Rust tem mantido o posto de linguagem mais amada segundo a avaliação anual do Stack Overflow desde 2016.

### 1.3. Características Técnicas
Rust é uma linguagem rápida que utiliza de diversas funcionalidades para poder ofertar essa performance. Estas são:

1.  Ela é uma linguagem que não utiliza maquinas virtuais, compilando seu código diretamente para o código da máquina. Evitando o uso de intermediários que poderiam reduzir a performance. [[9]](http://kmcallister.github.io/talks/rust/2015-contributing-to-rust/slides.html)

2. Rust não utiliza coletores de lixo em tempo de execução, calculando os tempos de vida durante a compilação. Evitando de ter gastos extras com memoria ou redução da velocidade.  [[10]](https://www.kentik.com/blog/under-the-hood-how-rust-helps-keep-kentik's-performance-on-high/)[[6]](https://www.rust-lang.org/pt-BR)

3. É uma linguagem que utiliza do principio de abstração sem custo, quase todos os tipos de abstrações são convertidos para código de máquina otimizado. [[11]](https://boats.gitlab.io/blog/post/zero-cost-abstractions/)

4. Rust tem o principio de segurança de thread, verificando em período de compilações erros e atestando que não irá
ocorrer problemas relativos a elas. [[12]](https://blog.rust-lang.org/2015/04/10/Fearless-Concurrency.html)

5.  Ela também é uma linguagem que tem princípios de baixo nível, permitindo o uso de otimizações e controles de memória pelo programador.

6. Rust utiliza vários métodos para garantir segurança de memória, assegurando durante tempo de compilação que não seja possível acessar locais das memorias depois de seu tempo de vida, variáveis não inicializadas, overflow de buffer, entre outros. Esses processos diferentes de C, muitos são feitos diretamente pelo compilador, não necessitando de validações ou adições manuais.

7. Rust tem um compilador que é capaz de identificar muitos erros durante o tempo de compilação. Evitando paradas e quebras de programa com a descoberta de erros [[14]](https://doc.rust-lang.org/book/ch09-00-error-handling.html)

## 2. Desafio Rust vs Go – Diferenças de DNA
### 2.1. Desafio
### 2.1.1. Idealização

O DNA é composto por inúmeros nucleotídeos, sendo eles dos tipos A G, C, T e U. Costuma-se escrever a sequência de DNA como um conjunto dessas letras. Contudo, durante as multiplicações das células alguns erros ocorrem e mudanças são passadas. É necessário então analisar as duas sequencias e identificar cada diferença entre elas.

Exemplo:
GAGCCTACTAACGGGAT
CATCGTAATGACGGCCT

Existem 7 diferenças entre as faixas.

### 2.1.2.Explicação técnica

O programa deverá ler dois arquivos .txt que contém a primeira e segunda sequência. Ambos arquivos conterão apenas letras (A, G, C, T e U). A partir disso o programa deve comparar as mesmas e identificar as diferenças entre elas, apenas contando cada diferença e retornando um inteiro com o valor final. Os dois arquivos fornecidos tem o mesmo número de caracteres, qualquer teste usado para simular deve ter o mesmo número de caracteres para cada uma das sequencias.
Cada arquivo consiste em 190.000.020 caracteres, contendo 9.500.001 diferenças entre eles. Cada linha tem 60 caracteres e 3 diferenças em cada. Para questão de testes recomenda o uso de apenas algumas linhas iniciais para uma verificação manual fácil. Pode se usar outras sequências para validação de teste.

Primeira Sequência:
https://gitlab.com/lpp-es-2020-1/rust/desafio/-/blob/master/desafio/Primeira%20Sequencia.txt

Segunda Sequência:
https://gitlab.com/lpp-es-2020-1/rust/desafio/-/blob/master/desafio/Segunda%20Sequencia.txt

Além de examinar o número de diferenças o programa deve realizar esse processo de duas formas. A primeira sem usar thread e a segunda usando 4 threads. Para cada execução o programa deve retornar o numero de erros identificados e o tempo gasto para realizar as comparações.

Exemplo de entradas:
Arquivo “Primeira Sequencia.txt”:
GGCATTACAGGCGTGAGCCTCCGCGCCCGGCCTTTTTGAGACGGAGTCTCGTTCTGTCGC
CCTGGCTGGAGTGCAGTGGTGCGATCTCGGCTCACTGCAACCTCCGCCTCCTGGGTTCAA
...
Arquivo “Segunda Sequencia.txt”:
GGGATTACAGGCGTGAGCCACCGCGCCCGGCCTTTTTGAGACGGAGTCTCGCTCTGTCGC
CCAGGCTGGAGTGCAGTGGCGCGATCTCGGCTCACTGCAACCTCCGCCTCCCGGGTTCAA
...

Exemplo de saída:

Teste sem uso de thread

Numero de diferenças: 9500001

Tempo de execução: 3534

Teste com uso de 4 threads

Numero de diferenças: 9500001

Tempo de execução: 3734

Exemplos que podem ser usados para testes e seus resultados esperados:
"", "" 0
"A", "A" 0
"G", "T" 1
"GGACTGAAATCTG", "GGACTGAAATCTG" 0
"GGACTGA", "GGACTGA" 0
"ACT", "GGA" 3
"GGACG", "GGTCG" 1
"ACCAGGG", "ACTATGG" 2
"GGACGGATTCTG", "AGGACGGATTCT" 9

### 2.2. Motivação:

Rust e Go são linguagem com focos relativamente distintos. Rust é uma linguagem complexa que foca em eficiência e segurança de memória, útil para serviços com equipe experiente e que necessitem de otimizações. Enquanto isso, Go foca em simplicidade e concorrência, útil para serviços com multi-tread e para utilização em equipes grandes e com necessidade de contratações.
A motivação desse desafio vem das diferenças de performance de Rust e Go, assim como em concorrência e simplicidade. Esse desafio irá focar nesses 3 tópicos. A necessidade de realizar os testes com e sem concorrência irá permitir notar qual tem uma concorrência mais otimizada e qual tem maior performance. O esperado é que rust possa realizar de forma mais rápida sem o uso de threads, mas que o ganho em performance no rust seja maior em Go.
O ultimo ponto a ser focado no desafio é a complexidade do código para realizar tais funções. Go tem muita facilidade em iniciar o uso de threads e é uma linguagem mais simples. Rust apresenta mais nuances e dificuldades impostas para realizar tarefas mais simples.

### 2.3. Instalação

Para realizar a instalação e execução do programa é necessário seguir os passos a seguir:

#### 1. Opção de clonar repositório
1.1. Clonar o repositório do seguinte link: https://gitlab.com/lpp-es-2020-1/rust/desafio/

1.2. Instalar rust: https://www.rust-lang.org/tools/install

1.3. Rodar o comando “cargo run” na pasta .../desafio/desafio

#### 2. Docker
2.1. Instalar Docker

2.2. Baixar imagem pelo Docker hub: https://hub.docker.com/r/schleogl10/desafio/tags

2.3. Rodar o docker

2.4. Realizando Testes

Para a realização de testes de velocidade, basta executar o programa que ele irá realizar a comparação entre os dois arquivos de 190.000.020 caracteres.
Para realização de testes customizados alterar os arquivos para as sequências desejadas:
• desafio\Primeira Sequencia.txt
• desafio\Primeira Sequencia.txt
Caso realizada alguma alteração nos arquivos de texto, certificar que ambos permanecem com o mesmo numero de caracteres.

### 2.5. Resultados

Com a finalização da criação do programa, ficou muito claro que rust exige muitas medidas de proteção e alterações para usar threads em comparação com o uso normal. Foi apresentado um ganho de performance correspondente ao número de threads usadas a mais (usando 4 threads, demorou apenas ¼ do tempo).

| Tentativa    | Numero de Core        | Tempo(ms)   |
| ------------- |:-------------:| -----:|
| 1      | 1     | 109  |
| 1      | 4     |   29 |
| 2      | 1     | 121  |
| 2      | 4     |   28 |
| 3      | 1     | 117  |
| 3      | 4     |   28 |
| 4      | 1     | 118  |
| 4      | 4     |   29 |
| 5      | 1     | 115  |
| 5      | 4     |   29 |


O tempo médio de execução em rust sem uso de concorrência foi de: 0.116 segundos
O tempo médio de execução usando 4 threads em rust foi de: 0.029 segundos

## 3. Referencias

1. https://exercism.io/my/solutions/52fd5414f21f41a7b615e73fef8d4a0d
2. https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/fasta.html
3. https://en.wikipedia.org/wiki/Rust_(programming_language)#:~:text=The%20language%20gre
4. w%20out%20of,and%20announced%20it%20in%202010.
5. https://doc.rust-lang.org/book/ch00-00-introduction.html
6. https://www.rust-lang.org/pt-BR
7. https://kornel.ski/rust-c-speed
8. http://kmcallister.github.io/talks/rust/2015-contributing-to-rust/slides.html
9. https://doc.rust-lang.org/book/ch09-00-error-handling.html
10. https://www.kentik.com/blog/under-the-hood-how-rust-helps-keep-kentik's-performance-on-high/
11. https://boats.gitlab.io/blog/post/zero-cost-abstractions/
12. https://blog.rust-lang.org/2015/04/10/Fearless-Concurrency.html
13. https://rustc-dev-guide.rust-lang.org/overview.html
14. https://doc.rust-lang.org/book/ch09-00-error-handling.html


## 4. Código-Fonte

https://gitlab.com/lpp-es-2020-1/rust/desafio/-/blob/master/desafio/src/main.rs

```Rust
let comeco = Instant::now();
    let mut count = 0;
    let mut diferencas = 0;
    for seq in contents1bytes {
        if *seq != contents2bytes[count] {
            diferencas = diferencas + 1;
        }
         count = count + 1;
    }
```
Como visto a cima o Código é consideravelmente simples, os grandes impactos na velocidade se darão pela otimização dos pequenos componentes como comparações, somas e iterações repetidas por um nomero muito grande de vezes.

A utilização de thread em rust é iniciada por thread::spawn que gera uma nova thread que ira ocorrer de forma concorrente com a thread principal.
```Rust
thread::spawn(move || {})
```
Para realizar acesso a variaveis fora das threads rust obriga que seja apenas utilizado mensagens. Para alterar variaveis fora da thread é necessário marcar como "unsafe" e clonar as variaveis que desejam ser alteradas:
```Rust
let counter1 = Arc::clone(&counter);
```
A alteração é realizado por um "lock" e "unwarp"
```Rust
let mut num = counter1.lock().unwrap();
```
Dessa forma é possivel alterar variaveis fora da thread, assim podendo passar o contador para fora da thread.

# Contra Desafio #

## Clojure vs Escala ##

## Analise ##

Em rust devido a forma como é estabelecido o Mutex e locks não é possivel utilizar a solução de semaphoro que geraria resultados de todos os filosofos se alimentarem de forma igual e similar a media de execuções. Isso ocorre por não ter como dar Lock em duas variaveis ao mesmo tempo. É possivel concluir que isso torna o acesso a duas variaveis simultaneamente algo um pouco mais complicado ou instavel já que não tem como se assegurar que a segunda estará disponivel depois de acessar a primeira mesmo que estejam disponiveis antes.
A forma de acesso a variaveis fora de uma thread permite evitar problemas simples, mas a forma como é estabelecido as funcionalidades de Rust acaba gerando uma grande desigualdade, como apresentado nos Resultados. Contudo o problema é possivel de ser realizado devido ao suporte a Mutex e concorrência. 

## Resultado ##

A realização dos testes foi feita ao realizar o programa por 10 vezes, a baixo está os valores de cada uma para cada um dos individuos e a media. No caso de execução usado foi usado um tempo randomico de espera entre 0 e 30 milisegundos para comer e pensar. E as execuções ocorriam por 30 segundos durante as threads. Após isso é retornado o valor de vezes que cada individuo comeu. No caso "Italo" e "Arthur" são filosofos que buscam o garfo da direita primeiro, enquanto os outros 3 buscam o da esquerda. O cenário ideal é onde todos filosofos se alimentem de forma igual.


| Iteração    | Felipe   | Emanuel | Italo | Marcelo | Arthur |
| ----------- | --------:|--------:|:-----:| -------:|-------:|
| 1           | 157      |    598  |  319  |   277   |   184  |
| 2           | 110      |    582  |  335  |   308   |   75   |
| 3           | 154      |    626  |  267  |   498   |   126  |
| 4           | 105      |    636  |  328  |   338   |   176  |
| 5           | 156      |    654  |  240  |   437   |   114  |
| 6           | 150      |    584  |  358  |   271   |   209  |
| 7           | 159      |    563  |  365  |   330   |   223  |
| 8           | 157      |    574  |  343  |   308   |   180  |
| 9           | 117      |    557  |  365  |   307   |   180  |
| 10          | 145      |    636  |  257  |   381   |   149  |

Como visto os casos não foram muito proximos, havendo claramente casos onde um se alimentou mais que outro. Contudo não houve casos de starvation ou deadlock.

## Comparação ##

Não foi apresentado resultados pelos criadores do desafio até o momento

### Link para Docker ###
https://hub.docker.com/repository/docker/schleogl10/contradesafio
